# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2021-06-22)


### Features

* add standard release support! ([bd9271f](https://gitlab.com/first_test-group/first-project/commit/bd9271f0de5e451c290f75a0b29663914c453706))


### Bug Fixes

* create nodejs application ([6c8245d](https://gitlab.com/first_test-group/first-project/commit/6c8245d88a340a536a66c70514317d864ff40fa2))
